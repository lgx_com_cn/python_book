# set(集合)数据结构
# set(集合)是一个非常有用的数据结构。它与列表(list)的行为类似，区别在于set不能包含重复的值。

some_list = ['a', 'b', 'c', 'b', 'd', 'c', 'n', 'n']
# 输出列表中重复的元素是啥
duplicates = set([x for x in some_list if some_list.count(x) > 1])
print(duplicates)

# set集合中的其它方法
# 交集，差集等等
valid = set(['yellow', 'red', 'blue', 'green', 'black'])
input_set = set(['red', 'brown'])

# 交集
intersection = input_set.intersection(valid)
print(intersection)

# 差集
difference = valid.difference(input_set)
print(difference)