# 虚拟环境(virtualenv)

#  pip install virtualenv
# 最重要的命令是：
# $ virtualenv myproject
# $ source bin/activate
#
# 如果你想让你的virtualenv使用系统全局模块，请使用--system-site-packages参数创建你的virtualenv，例如：
#
# virtualenv --system-site-packages mycoolproject
# 使用以下命令可以退出这个virtualenv:
#
# $ deactivate
# 运行之后将恢复使用你系统全局的Python模块