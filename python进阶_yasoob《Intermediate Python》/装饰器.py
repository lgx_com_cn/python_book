# 装饰器(Decorators)是Python的一个重要部分。简单地说：他们是修改其他函数的功能的函数。他们有助于让我们的代码更简短，
# 也更Pythonic（Python范儿）。大多数初学者不知道在哪儿使用它们，所以我将要分享下，哪些区域里装饰器可以让你的代码更简洁。
# 首先，让我们讨论下如何写你自己的装饰器。
# 这可能是最难掌握的概念之一。我们会每次只讨论一个步骤，这样你能完全理解它


# 1. 一切皆对象
def hi():
    print(6666666)
greet = hi
del hi
greet()
# hi()

# 2. 在函数中定义函数
def hi(name="yasoob"):
    print("now you are inside the hi() function")

    def greet():
        return "now you are in the greet() function"

    def welcome():
        return "now you are in the welcome() function"

    print(greet())
    print(welcome())
    print("now you are back in the hi() function")
hi()
#output:now you are inside the hi() function
#       now you are in the greet() function
#       now you are in the welcome() function
#       now you are back in the hi() function

# 上面展示了无论何时你调用hi(), greet()和welcome()将会同时被调用。
# 然后greet()和welcome()函数在hi()函数之外是不能访问的，比如：

# greet()
#outputs: NameError: name 'greet' is not defined


# 3. 在函数中返回函数


# 4. 将函数作为参数传递给另一个函数
def hi():
    return "hi yasoob!"

def doSomethingBeforeHi(func):
    print("I am doing some boring work before executing hi()")
    print(func())

doSomethingBeforeHi(hi)
#outputs:I am doing some boring work before executing hi()
#        hi yasoob!



# 5. 第一个装饰器

def a_new_decorator(a_func):
    def wrapTheFunction():
        print("装饰操作......前")
        a_func()  # 执行被装饰的函数
        print("装饰操作......后")
        pass
    return wrapTheFunction
    pass

def require_decorator_func():
    print("需要被装饰的函数.......")

# require_decorator_func()

# 原始的方式生成装饰函数
# require_decorator_func = a_new_decorator(require_decorator_func)
#
# require_decorator_func()


# 使用@符号生成装饰函数
@a_new_decorator
def a_function_requiring_decoration():
    """Hey you! Decorate me!"""
    print("I am the function which needs some decoration to "
          "remove my foul smell")

a_function_requiring_decoration()

print(a_function_requiring_decoration.__name__)
# Output: wrapTheFunction
# 这并不是我们想要的！Ouput输出应该是“a_function_requiring_decoration”。
# 这里的函数被warpTheFunction替代了。它重写了我们函数的名字和注释文档(docstring)。
# 幸运的是Python提供给我们一个简单的函数来解决这个问题，那就是functools.wraps


# 规范化的装饰器
from functools import wraps

def decorator_name(f):
    # @wraps接受一个函数来进行装饰,并加入了复制函数名称、注释文档、参数列表等等的功能。这可以让我们在装饰器里面访问在装饰之前的函数的属性
    @wraps(f)
    def decorated(*args, **kwargs):
        if not can_run:
            print("Function will not run")
            return
        print("装饰操作......前>>>>>>>>>>>>>",)
        f(*args, **kwargs)
        print("装饰操作......后>>>>>>>>>>>>>")
        pass
    return decorated

@decorator_name
def requireDecoratorFunc():
    age = 18
    print("需要装饰的函数......>>>>>>>>>>>>>")


can_run = True
requireDecoratorFunc()
print(requireDecoratorFunc.__name__)


# 6. 使用场景
# 6.1 授权(Authorization)
# 装饰器能有助于检查某个人是否被授权去使用一个web应用的端点(endpoint)。它们被大量使用于Flask和Django web框架中。这里是一个例子来使用基于装饰器的授权

# from functools import wraps
# def requires_auth(f):
#     @wraps(f)
#     def decorated(*args, **kwargs):
#         auth = request.authorization
#         if not auth or not check_auth(auth.username, auth.password):
#             authenticate()
#         return f(*args, **kwargs)
#     return decorated

# 6.2 日志(Logging)
# 日志是装饰器运用的另一个亮点。这是个例子：

from functools import wraps
def logit(func):
    @wraps(func)
    def with_logging(*args, **kwargs):
        print(func.__name__ + " was called。。。。。。。。。。。。。。llll")
        return func(*args, **kwargs)
    return with_logging

@logit
def addition_func(x):
   """Do some math."""
   return x + x


result = addition_func(4)
# Output: addition_func was called


# 7. 带参数的装饰器
# 来想想这个问题，难道@wraps不也是个装饰器吗？但是，它接收一个参数，就像任何普通的函数能做的那样。那么，为什么我们不也那样做呢？
# 这是因为，当你使用@my_decorator语法时，你是在应用一个以单个函数作为参数的一个包裹函数。记住，
# Python里每个东西都是一个对象，而且这包括函数！记住了这些，我们可以编写一下能返回一个包裹函数的函数。

# 7.1 在函数中嵌入装饰器
from functools import wraps

def logit(logfile='out.log'):
    def logging_decorator(func):
        @wraps(func)
        def wrapped_function(*args, **kwargs):
            log_string = func.__name__ + " was called"
            print(log_string)
            # 打开logfile，并写入内容
            with open(logfile, 'a') as opened_file:
                # 现在将日志打到指定的logfile
                opened_file.write(log_string + '\n')
            return func(*args, **kwargs)
        return wrapped_function
    return logging_decorator

@logit()
def myfunc1():
    pass

myfunc1()
# Output: myfunc1 was called
# 现在一个叫做 out.log 的文件出现了，里面的内容就是上面的字符串

@logit(logfile='func2.log')
def myfunc2():
    pass

myfunc2()
# Output: myfunc2 was called
# 现在一个叫做 func2.log 的文件出现了，里面的内容就是上面的字符串

# 7.2 装饰器类
# 幸运的是，类也可以用来构建装饰器。那我们现在以一个类而不是一个函数的方式，来重新构建logit。

from functools import wraps

class logit(object):
    def __init__(self, logfile='out.log'):
        self.logfile = logfile

    def __call__(self, func):
        @wraps(func)
        def wrapped_function(*args, **kwargs):
            log_string = func.__name__ + " was called"
            print(log_string)
            # 打开logfile并写入
            with open(self.logfile, 'a') as opened_file:
                # 现在将日志打到指定的文件
                opened_file.write(log_string + '\n')
            # 现在，发送一个通知
            self.notify()
            return func(*args, **kwargs)
        return wrapped_function

    def notify(self):
        # logit只打日志，不做别的
        pass
# 这个实现有一个附加优势，在于比嵌套函数的方式更加整洁，而且包裹一个函数还是使用跟以前一样的语法：

@logit()
def myfunc1():
    pass
# 现在，我们给logit创建子类，来添加email的功能(虽然email这个话题不会在这里展开)。

class email_logit(logit):
    '''
    一个logit的实现版本，可以在函数调用时发送email给管理员
    '''
    def __init__(self, email='admin@myproject.com', *args, **kwargs):
        self.email = email
        super(logit, self).__init__(*args, **kwargs)

    def notify(self):
        # 发送一封email到self.email
        # 这里就不做实现了
        pass
# 从现在起，@email_logit将会和@logit产生同样的效果，但是在打日志的基础上，还会多发送一封邮件给管理员。