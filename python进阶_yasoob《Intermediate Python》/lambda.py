# lambda表达式
# lambda表达式是一行函数。
# 它们在其他语言中也被称为匿名函数。如果你不想在程序中对一个函数使用两次，你也许会想用lambda表达式，它们和普通的函数完全一样。
#
# 原型
#     lambda 参数:操作(参数)

# 列表排序
a = [(1, 2), (4, 1), (9, 10), (13, -3)]
a.sort(key=lambda x: x[1])

print(a)
# Output: [(13, -3), (4, 1), (1, 2), (9, 10)]
# 列表并行排序

data = zip([1,2,3], [4,5,6,])
# data.sort()
list1, list2 = map(lambda t: list(t), zip(*data))
print(list1,)
print(list2)