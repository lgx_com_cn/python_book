# open函数
# open 函数可以打开一个文件。超级简单吧？大多数时候，我们看到它这样被使用：
#
# f = open('photo.jpg', 'r+')
# jpgdata = f.read()
# f.close()
# 我现在写这篇文章的原因，是大部分时间我看到open被这样使用。有三个错误存在于上面的代码中