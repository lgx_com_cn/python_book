# Map，Filter 和 Reduce 三个函数能为函数式编程提供便利。我们会通过实例一个一个讨论并理解它们。

# Map会将一个函数映射到一个输入列表的所有元素上。这是它的规范：
# 规范
# map(function_to_apply, list_of_inputs)

#把一个列表元素一个个传递给函数，并收集输出 传统做法
items = [1, 2, 3, 4, 5]
squared = []
for item in items:
    squared.append(item**2)
    pass
# 使用map
squared = list(map(lambda x: x**2, items))






# Filter
# 过滤列表中的元素
number_list = range(-5, 5)
less_than_2 = filter(lambda x: x < 2, number_list)
print(list(less_than_2))





# Reduce
# 列表中逐个元素的叠加，叠成计算等
from  functools import reduce
sum = reduce(lambda x, y: x+y, [1, 2, 3, 4, 5])
mul = reduce(lambda x, y: x*y, [1, 2, 3, 4, 5])
print(sum)
print(mul)




