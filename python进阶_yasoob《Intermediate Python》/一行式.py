# 本章节,我将向大家展示一些一行式的Python命令

# 1. 简易Web Server
# python -m http.server

# 2. 漂亮的打印
from pprint import pprint
my_dict = {'name': 'Yasoob', 'age': 'undefined', 'personality': 'awesome'}
pprint(my_dict)
# cat file.json | python -m json.tool
# # 脚本性能分析 这可能在定位你的脚本中的性能瓶颈时，会非常奏效：
# #
# #     python -m cProfile my_script.py
# # 备注：cProfile是一个比profile更快的实现，因为它是用c写的

# 3. CSV转换为json
#     python -c "import csv,json;print json.dumps(list(csv.reader(open('csv_file.csv'))))"

# 4. 列表辗平
# 您可以通过使用itertools包中的itertools.chain.from_iterable轻松快速的辗平一个列表。下面是一个简单的例子：
import itertools
a_list = [[1, 2], [3, 4], [5, 6]]
print(list(itertools.chain.from_iterable(a_list)))
# Output: [1, 2, 3, 4, 5, 6]
# or
print(list(itertools.chain(*a_list)))

# 5.一行式的构造器
# 避免类初始化时大量重复的赋值语句
class A(object):
    def __init__(self, a, b, c, d, e, f):
        self.__dict__.update({k: v for k, v in locals().items() if k != 'self'})