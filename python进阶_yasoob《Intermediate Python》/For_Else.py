fruits = ['apple', 'banana', 'mango']
for fruit in fruits:
    print(fruit.capitalize())

# else从句
# for循环还有一个else从句，我们大多数人并不熟悉。这个else从句会在循环正常结束时执行。这意味着，循环没有遇到任何break. 一旦你掌握了何时何地使用它，它真的会非常有用
# 这就是for/else循环的基本结构：
# for item in container:
#     if search_something(item):
#         # Found it!
#         process(item)
#         break
# else:
#     # Didn't find anything..
#     not_found_in_container()

for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
            print(n, 'equals', x, '*', n/x)
            break
    else:
        # loop fell through without finding a factor
        print(n, 'is a prime number')