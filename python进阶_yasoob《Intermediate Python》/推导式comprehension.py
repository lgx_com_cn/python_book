# 各种推导式(comprehensions)
# 推导式（又称解析式）是Python的一种独有特性，如果我被迫离开了它，我会非常想念。推导式是可以从一个数据序列构建另一个新的数据序列的结构体。 共有三种推导，在Python2和3中都有支持：
#
# 列表(list)推导式
# 字典(dict)推导式
# 集合(set)推导式



# 1. 列表(list)推导式
# 列表推导式（又称列表解析式）提供了一种简明扼要的方法来创建列表。
# 它的结构是在一个中括号里包含一个表达式，然后是一个for语句，然后是0个或多个for或者if语句。那个表达式可以是任意的，
# 意思是你可以在列表中放入任意类型的对象。返回结果将是一个新的列表，在这个以if和for语句为上下文的表达式运行完成之后产生。

# 规范
variable = [out_exp for out_exp in [1,5,6,2,5,2,9,8] if out_exp == 2]
# 这里是另外一个简明例子:

multiples = [i for i in range(30) if i % 3 == 0]
print(multiples)




# 2.字典推导式（dict comprehensions）
# 字典推导和列表推导的使用方法是类似的。这里有个我最近发现的例子：

mcase = {'a': 10, 'b': 34, 'A': 7, 'Z': 3}

mcase_frequency = {
    k.lower(): mcase.get(k.lower(), 0) + mcase.get(k.upper(), 0)
    for k in mcase.keys()
}

# mcase_frequency == {'a': 17, 'z': 3, 'b': 34}
# 在上面的例子中我们把同一个字母但不同大小写的值合并起来了。
# 你还可以快速对换一个字典的键和值：
some_dict= {"a":'china', 'b':'japan'}
newdict = {v: k for k, v in some_dict.items()}
print(newdict)



# 3. 集合推导式（set comprehensions）
# 它们跟列表推导式也是类似的。 唯一的区别在于它们使用大括号{}。 举个例子：

squared = {x**2 for x in [1, 1, 2]}
print(squared)
# Output: {1, 4}
