# 自省(introspection)，在计算机编程领域里，是指在运行时来判断一个对象的类型的能力。
# 它是Python的强项之一。Python中所有一切都是一个对象，而且我们可以仔细勘察那些对象。
# Python还包含了许多内置函数和模块来帮助我们。


# 1. dir
# 它是用于自省的最重要的函数之一。它返回一个列表，列出了一个对象所拥有的属性和方法。这里是一个例子：
my_list = [1, 2, 3]
a = dir(my_list)
print(a)

# 2. type和id
# type函数返回一个对象的类型。举个例子：
print(type(''))
# Output: <type 'str'>
print(type([]))
# Output: <type 'list'>
print(type({}))
# Output: <type 'dict'>
print(type(dict))

# id()函数返回任意不同种类对象的唯一ID，举个例子：
name = "Yasoob"
print(id(name))
print(id({}))


# 3.inspect模块
# inspect模块也提供了许多有用的函数，来获取活跃对象的信息。比方说，你可以查看一个对象的成员，只需运行：

import inspect
print(inspect.getmembers(str))